:hivemq-link: http://www.hivemq.com
:hivemq-extension-docs-link: http://www.hivemq.com/docs/extensions/latest/
:hivemq-extension-docs-archetype-link: http://www.hivemq.com/docs/extensions/latest/#maven-archetype-chapter
:hivemq-blog-tools: http://www.hivemq.com/mqtt-toolbox
:maven-documentation-profile-link: http://maven.apache.org/guides/introduction/introduction-to-profiles.html
:hivemq-support: http://www.hivemq.com/support/
:hivemq-listener: https://www.hivemq.com/docs/hivemq/4.4/user-guide/listeners.html#tcp-listener

== HiveMQ OAuth2 Role based Access Control Extension

*Extension Type*: Security

*Version*: 2.0.0

*License*: Apache License 2.0

=== Disclaimer

This project is a fork of the https://github.com/hivemq/hivemq-file-rbac-extension[hivemq-file-rbac-extension]. What are the differences ?

* The password used for authentication is a bearer Json Web Token (JWT)
* The token is verified by calling an oidc server
* Authorization rules are configured in esternal database (couchbase v 6.6.0 is currently supported)
* Environment variables are used to store configuration. This is more suitable when using docker containers.
* XML configuration files has been removed
* Use of mutation testing


=== Purpose

The OAuth2 Authentication and Authorization Extension implements Access Control based on information extracted from JWT.

This extension implements the configuration for authentication and authorization rules.
These mechanisms are important to protect a MQTT deployment, and the data which is exchanged, from unwanted access.

The extension provides fine grained control on a topic level to limit clients to specific topics and specific actions (publish or subscribe). Substitution rules for clientId and username allow for dynamic roles to be applied to multiple clients, while still limiting each client to "their own" topics.

=== Features

* Username and password based authentication for MQTT Clients. The password is a JWT.
* Fine grained access control on a topic-filter level
* Role based permission management
* Automatic Substitution of client identifier and username
* Extraction of rbac configuration rules from Couchbase database
* Runtime reload for rbac-rules

NOTE: For docker's users, https://gitlab.com/william.younang/hivemq-ce[this repository] explain how to obtain a dockerised version of HiveMQ CE with some built-in extensions.

=== Installation
. Install couchbase v6.6.0 or higher
. Create the bucket hivemq_oauth2_rbac_config and insert some records as per the example in the configuration section
. Unzip the file: `hivemq-oauth2-rbac-extension-<version>-distribution.zip` to the directory: `<HIVEMQ_HOME>/extensions`
. Configure the extension by setting the required environment variables
. Start HiveMQ

=== First Steps

Install the extension. The roles from the bucket hivemq_oauth2_rbac_config are now applied to all new MQTT connections.

=== Next Steps

Setup your custom Roles in the rbac-rules configuration bucket and configure the extension for your specific use case.

CAUTION: Because client identifier and user names can be used for <<substitution,substitution>> in the permissions, MQTT wildcard characters `#` and  `+` are prohibited for client identifier and user names when this extension is used. MQTT connections which include these characters are denied by this extension.

[#configuration]
=== Configuration

This extension does not have configuration file, environment variables are used.
The configuration of Roles and Permissions is in an esternal database.

The rbac-rules table is watched for changes and reloaded at runtime if necessary. If the rbac-rules configuration table has changed and contains a valid configuration, then the previous configuration is automatically overrided.
If the new rbac-rules configuration is invalid the current configuration is maintained.

NOTE: The permissions for connected clients are not changed, only new connecting clients are affected.

[#rbac-rules-config]
==== rbac-rules table sample queries

.Example of couchbase queries
[source,sql]
----
CREATE PRIMARY INDEX rbac_primar_index ON hivemq_oauth2_rbac_config;

INSERT INTO hivemq_oauth2_rbac_config (KEY, VALUE)
    VALUES ( "ADMIN",
              { "id": "ADMIN",
                "permissions" : [
                {
                "topic":"outgoing/${{clientid}}",
                "activity":"PUBLISH",
                "retain":"RETAINED"
                },
                {
                "topic":"incoming/${{username}}/actions",
                "activity":"SUBSCRIBE"
                },
                {
                "topic":"data/${{clientid}}/#"
                }
                ]
            } );

INSERT INTO hivemq_oauth2_rbac_config (KEY, VALUE)
    VALUES ( "DOCTOR4",
              { "id": "DOCTOR4",
                "permissions" : [
                {
                "topic":"#"
                }
                ]
            } );
----

===== Role configuration

|===
|Configuration |Description
|`id` |The ID for this role.
|`permissions` |A list of permissions which are applied for this role. Permissions are applied and checked by HiveMQ in the order the appear in the configuration bucket.
|===

===== Permisssion configuration

|===
|Configuration |Default |Description
|`topic` |-|The topic on which this permission should apply. Can contain standard MQTT wildcards `#` and `+`. Also special substitution with `${{clientid}}` and `${{username}}` is supported.
|`activity` |`ALL` |The activity which this client can perform on this topic. Can be `PUBLISH`, `SUBSCRIBE` or `ALL`.
|`qos` |`ALL` |The MQTT QoS which this client can publish/subscribe with on this topic. The value can be `ZERO`, `ONE`, `TWO`, `ZERO_ONE`, `ONE_TWO`, `ZERO_TWO` or `ALL`.
|`retained` |`ALL` |If a message published on this topic can/must be retained. Values are `NOT_RETAINED`, `RETAINED` or `ALL`. This setting is only relevant for PUBLISH messages.
|`shared-subscription` |`ALL` |If a subscription on this topic can/must be a shared subscription. Values are `SHARED`, `NOT_SHARED` or `ALL`. This setting is only relevant for SUBSCRIBE messages.
|`shared-group` |`#` |Limits the Shared Subscription group name for a subscription. Values are `#` to match all or a specific string value. This setting is only relevant for SUBSCRIBE messages that include a Shared Subscription.
|===

[#substitution]
===== Substitution
The special markers `${{clientid}}` and `${{username}}` in the topic filter for a permission are automatically replaced by the extension with the client identifier and username of the client for which authorization is performed. This allows to configure a permission that applies to multiple clients, but always contains their specific client identifier or username in the topic. Limiting each client to "their own" topics.

[#extensions-config]
==== Extension configuration

Extension is configured by setting to the following environment variables :


|===
|Environment Variable |Default |Description
|`RBAC_CONFIG_RELOAD_INTERVAL` |`70` |Regular interval in seconds, in which the rbac-rules configuration bucket is checked for changes and reloaded.
|`AUTH2_SERVER_URL` |`null` |URL of the OAuth2 Server.
|`AUTH2_CLIENT_ID` |`null` |The client's identifier configured at OAuth2 Server level.
|`AUTH2_REALM_ID` |`null` |The realm's identifier configured at OAuth2 Server level.
|`COUCHBASE_HOSTS` |`null` |A list of ip address for couchbase servers. A comma(,) must be used as separator. It is possible to specify only one ip address.
|`COUCHBASE_USERNAME` |`null` |The username, it should have read only grant on the bucket.
|`COUCHBASE_PASSWORD` |`null` |The password of the couchbase  bucket.
|`COUCHBASE_BUCKET` |`null` |The bucket's name.
|`COUCHBASE_ENABLE_SSL` |`false` |indicates if certificates are required for connection to couchbase.
|`COUCHBASE_KEYSTORE_FILE` |`null` |the link to keystore file. Used only if ssl is enabled.
|`COUCHBASE_KEYSTORE_PASSWORD` |`null` |The password of the keystore file. Used only if ssl is enabled.
|`COUCHBASE_TRUSTSTORE_FILE` |`null` |The link truststore file. Used only if ssl is enabled.
|`COUCHBASE_TRUSTSTORE_PASSWORD` |`null` |The truststore's password.
|===


=== TODO

* Increase code and mutation coverage to 100 %.
* Tests have been done only with https://www.keycloak.org/[Keycloak]. Further tests / developments should be done for other OIDC providers.


= Contributing

If you want to contribute to HiveMQ Oauth2 RBAC Extension, see the link:CONTRIBUTING.md[contribution guidelines].

= License

HiveMQ Oauth2 RBAC Extension is licensed under the `APACHE LICENSE, VERSION 2.0`. A copy of the license can be found link:LICENSE.txt[here].

