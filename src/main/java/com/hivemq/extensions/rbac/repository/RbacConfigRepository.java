package com.hivemq.extensions.rbac.repository;

import com.hivemq.extensions.rbac.configuration.entities.Role;

import java.util.List;

/**
 * @author William Younang
 **/
public interface RbacConfigRepository {
    List<Role> getAllRoles();
}
