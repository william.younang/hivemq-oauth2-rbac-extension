package com.hivemq.extensions.rbac.repository;

import com.couchbase.client.java.Bucket;
import com.couchbase.client.java.Cluster;
import com.couchbase.client.java.CouchbaseCluster;
import com.couchbase.client.java.document.json.JsonObject;
import com.couchbase.client.java.env.CouchbaseEnvironment;
import com.couchbase.client.java.env.DefaultCouchbaseEnvironment;
import com.couchbase.client.java.query.N1qlQuery;
import com.couchbase.client.java.query.N1qlQueryResult;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.hivemq.extension.sdk.api.annotations.NotNull;
import com.hivemq.extensions.rbac.configuration.entities.DatabaseConfig;
import com.hivemq.extensions.rbac.configuration.entities.Role;

import java.io.IOException;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * @author William Younang
 **/
public class CouchbaseRbacConfigRepository implements RbacConfigRepository {

    @NotNull String bucketName;
    @NotNull Bucket bucket;
    private final ObjectMapper objectMapper = new ObjectMapper();
    private String selectAllConfigQuery;

    public CouchbaseRbacConfigRepository(final DatabaseConfig databaseConfig) {
        final String[] hosts = databaseConfig.getHosts().split(",");
        final Cluster cluster = CouchbaseCluster.create(hosts);
        cluster.authenticate(databaseConfig.getDatabaseUserName(), databaseConfig.getDatabasePassword());
        bucket = cluster.openBucket(databaseConfig.getDatabaseTable());
        bucketName = databaseConfig.getDatabaseTable();
        selectAllConfigQuery = "SELECT * FROM ".concat(bucketName);
    }

    public CouchbaseRbacConfigRepository() {
        bucketName = System.getenv("COUCHBASE_BUCKET");
        final String[] hosts = System.getenv("COUCHBASE_HOSTS").split(",");
        final String sslEnabled = System.getenv("COUCHBASE_ENABLE_SSL");
        Cluster cluster;
        if (sslEnabled != null && Boolean.TRUE.equals(Boolean.parseBoolean(sslEnabled))) {
            final CouchbaseEnvironment environment = DefaultCouchbaseEnvironment
                    .builder()
                    .sslEnabled(true)
                    .sslKeystoreFile(System.getenv("COUCHBASE_KEYSTORE_FILE"))
                    .sslKeystorePassword(System.getenv("COUCHBASE_KEYSTORE_PASSWORD"))
                    .sslTruststoreFile(System.getenv("COUCHBASE_TRUSTSTORE_FILE"))
                    .sslTruststorePassword(System.getenv("COUCHBASE_TRUSTSTORE_PASSWORD"))
                    .build();
            cluster = CouchbaseCluster.create(environment, hosts);
        } else {
            cluster = CouchbaseCluster.create(hosts);
        }

        cluster.authenticate(System.getenv("COUCHBASE_USERNAME"), System.getenv("COUCHBASE_PASSWORD"));
        bucket = cluster.openBucket(bucketName);
        selectAllConfigQuery = "SELECT * FROM ".concat(bucketName);
    }

    @Override
    public List<Role> getAllRoles() {
        final N1qlQueryResult result = bucket.query(N1qlQuery.simple(selectAllConfigQuery));

        return result.allRows()
                .stream()
                .map(row -> {
                    try {
                        JsonObject rowJson = row.value();
                        return objectMapper.readValue(rowJson.get(bucketName).toString(), Role.class);
                    } catch (IOException e) {
                        return null;
                    }
                })
                .filter(Objects::nonNull)
                .collect(Collectors.toList());
    }
}
