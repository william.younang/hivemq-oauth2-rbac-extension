/*
 * Copyright 2018 dc-square GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.hivemq.extensions.rbac;

import com.hivemq.extension.sdk.api.ExtensionMain;
import com.hivemq.extension.sdk.api.annotations.NotNull;
import com.hivemq.extension.sdk.api.parameter.ExtensionStartInput;
import com.hivemq.extension.sdk.api.parameter.ExtensionStartOutput;
import com.hivemq.extension.sdk.api.parameter.ExtensionStopInput;
import com.hivemq.extension.sdk.api.parameter.ExtensionStopOutput;
import com.hivemq.extension.sdk.api.services.Services;
import com.hivemq.extensions.rbac.configuration.Configuration;
import com.hivemq.extensions.rbac.configuration.entities.ExtensionConfig;
import com.hivemq.extensions.rbac.repository.CouchbaseRbacConfigRepository;
import com.hivemq.extensions.rbac.repository.RbacConfigRepository;
import com.hivemq.extensions.rbac.utils.CredentialsValidator;
import com.hivemq.extensions.rbac.utils.KeycloakAccessTokenHandler;
import com.hivemq.extensions.rbac.utils.Oauth2AccessTokenHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Oauth2AuthMain implements ExtensionMain {

    private static final @NotNull Logger log = LoggerFactory.getLogger(Oauth2AuthMain.class);

    @Override
    public void extensionStart(final @NotNull ExtensionStartInput extensionStartInput, final @NotNull ExtensionStartOutput extensionStartOutput) {

        log.info("Starting Oauth2 RBAC extension.");

        try {

            final ExtensionConfig extensionConfiguration = new ExtensionConfig();
            extensionConfiguration.init();

            final RbacConfigRepository rbacConfigRepository = new CouchbaseRbacConfigRepository();

            final Configuration configuration = new Configuration(rbacConfigRepository, Services.extensionExecutorService(), extensionConfiguration);
            configuration.init();

            final Oauth2AccessTokenHandler oauth2AccessTokenHandler = new KeycloakAccessTokenHandler(Services.metricRegistry(), extensionConfiguration.getoAuth2Config());
            final CredentialsValidator credentialsValidator = new CredentialsValidator(configuration, oauth2AccessTokenHandler);

            credentialsValidator.init();

            Services.securityRegistry().setAuthenticatorProvider(new Oauth2AuthenticatorProvider(credentialsValidator));

        } catch (Exception e) {
            log.error("Exception thrown at extension start: ", e);
        }

    }

    @Override
    public void extensionStop(final @NotNull ExtensionStopInput extensionStopInput, final @NotNull ExtensionStopOutput extensionStopOutput) {
        log.info("Stopping Oauth2 RBAC extension.");
    }

}
