package com.hivemq.extensions.rbac.utils;

import com.codahale.metrics.MetricRegistry;
import com.hivemq.extension.sdk.api.annotations.NotNull;
import com.hivemq.extension.sdk.api.annotations.ThreadSafe;
import com.hivemq.extensions.rbac.configuration.entities.OAuth2Config;
import org.keycloak.TokenVerifier;
import org.keycloak.representations.AccessToken;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.security.PublicKey;
import java.util.Collections;
import java.util.Set;

/**
 * @author William Younang
 **/
@ThreadSafe
public class KeycloakAccessTokenHandler extends AbstractOauth2AccessTokenHandler {
	private static final @NotNull Logger log = LoggerFactory.getLogger(KeycloakAccessTokenHandler.class);

	public KeycloakAccessTokenHandler(@NotNull MetricRegistry metricRegistry, @NotNull final OAuth2Config oAuth2Config) {
		super(metricRegistry, oAuth2Config);
	}

	@Override
	public Set<String> getResourceAccessRoles(String jwtToken) {
		try {
			final TokenVerifier<AccessToken> accessTokenTokenVerifier = TokenVerifier.create(jwtToken, AccessToken.class);
			PublicKey publicKey = retrieveRSAPublicKeyFromCertsEndpoint(accessTokenTokenVerifier.getHeader().getKeyId());
			final AccessToken accessToken = accessTokenTokenVerifier.publicKey(publicKey).verify().getToken();
			if (!accessToken.isActive()) {
				log.warn("access token is expired");
				metricRegistry.meter(OAUTH2_TOKEN_VERIFICATION_FAILURE).mark();
				return Collections.emptySet();
			}
			final AccessToken.Access access = accessToken.getResourceAccess().get(clientId);
			metricRegistry.meter(OAUTH2_TOKEN_VERIFICATION_SUCCESS).mark();
			return access.getRoles();
		} catch (Exception e) {
			log.error("error occurs while retrieving roles from access token", e);
			metricRegistry.meter(OAUTH2_TOKEN_VERIFICATION_FAILURE).mark();
			return Collections.emptySet();
		}
	}
}
