package com.hivemq.extensions.rbac.utils;

import java.util.Set;
/**
 * @author William Younang
 **/
public interface Oauth2AccessTokenHandler {
     Set<String> getResourceAccessRoles(String jwtToken);
}
