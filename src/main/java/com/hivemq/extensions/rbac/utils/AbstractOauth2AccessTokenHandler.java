package com.hivemq.extensions.rbac.utils;

import com.codahale.metrics.MetricRegistry;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.hivemq.extension.sdk.api.annotations.NotNull;
import com.hivemq.extensions.rbac.configuration.entities.OAuth2Config;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigInteger;
import java.net.URL;
import java.security.KeyFactory;
import java.security.PublicKey;
import java.security.spec.RSAPublicKeySpec;
import java.util.Base64;
import java.util.List;
import java.util.Map;
/**
 * @author William Younang
 **/
public abstract class AbstractOauth2AccessTokenHandler implements Oauth2AccessTokenHandler {

    protected static final String OAUTH2_TOKEN_VERIFICATION_FAILURE = "com.hivemq.extensions.oauth2-rbac.verification.failure";
    protected static final String OAUTH2_TOKEN_VERIFICATION_SUCCESS = "com.hivemq.extensions.oauth2-rbac.verification.success";
    private static final @NotNull Logger log = LoggerFactory.getLogger(AbstractOauth2AccessTokenHandler.class);

    protected final @NotNull MetricRegistry metricRegistry;
    protected final String realmUrlCerts;
    protected final String clientId;
    protected final ObjectMapper objectMapper = new ObjectMapper();

    public AbstractOauth2AccessTokenHandler(@NotNull MetricRegistry metricRegistry, @NotNull final OAuth2Config oAuth2Config) {
        this.metricRegistry = metricRegistry;
        this.realmUrlCerts = oAuth2Config.getOauth2ServerUrl()
                .concat("/realms/")
                .concat(oAuth2Config.getOauth2RealmId())
                .concat("/protocol/openid-connect/certs");
        this.clientId = oAuth2Config.getOauth2ClientId();
    }

    protected PublicKey retrieveRSAPublicKeyFromCertsEndpoint(final String keyId) {
        try {
            @SuppressWarnings("unchecked")
            Map<String, Object> certInfos = objectMapper.readValue(new URL(realmUrlCerts).openStream(), Map.class);

            List<Map<String, Object>> keys = (List<Map<String, Object>>) certInfos.get("keys");

            Map<String, Object> keyInfo = null;
            for (Map<String, Object> key : keys) {
                String kid = (String) key.get("kid");

                if (keyId.equals(kid)) {
                    keyInfo = key;
                    break;
                }
            }

            if (keyInfo == null) {
                return null;
            }

            KeyFactory keyFactory = KeyFactory.getInstance("RSA");
            String modulusBase64 = (String) keyInfo.get("n");
            String exponentBase64 = (String) keyInfo.get("e");

            Base64.Decoder urlDecoder = Base64.getUrlDecoder();
            BigInteger modulus = new BigInteger(1, urlDecoder.decode(modulusBase64));
            BigInteger publicExponent = new BigInteger(1, urlDecoder.decode(exponentBase64));

            return keyFactory.generatePublic(new RSAPublicKeySpec(modulus, publicExponent));

        } catch (Exception ex) {
            log.error("Error occurs while retrieving public certificate", ex);
            return null;
        }
    }

}
