/*
 * Copyright 2018 dc-square GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.hivemq.extensions.rbac.utils;

import com.hivemq.extension.sdk.api.annotations.NotNull;
import com.hivemq.extension.sdk.api.annotations.Nullable;
import com.hivemq.extension.sdk.api.annotations.ThreadSafe;
import com.hivemq.extension.sdk.api.auth.parameter.TopicPermission;
import com.hivemq.extension.sdk.api.services.builder.Builders;
import com.hivemq.extensions.rbac.configuration.Configuration;
import com.hivemq.extensions.rbac.configuration.entities.Permission;
import com.hivemq.extensions.rbac.configuration.entities.Role;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.nio.ByteBuffer;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

@ThreadSafe
@SuppressWarnings("ConstantConditions")
public class CredentialsValidator {
    private static final @NotNull Logger log = LoggerFactory.getLogger(CredentialsValidator.class);

    private final @NotNull Configuration configuration;

    private final ReadWriteLock oidcServerLock = new ReentrantReadWriteLock();
    private final ReadWriteLock rolesLock = new ReentrantReadWriteLock();

    private final @NotNull Oauth2AccessTokenHandler oauth2AccessTokenHandler;

    private @NotNull Map<String, Role> roles = new ConcurrentHashMap<>();

    public CredentialsValidator(@NotNull final Configuration configuration, @NotNull final Oauth2AccessTokenHandler oauth2AccessTokenHandler) {
        this.configuration = configuration;
        this.oauth2AccessTokenHandler = oauth2AccessTokenHandler;
    }

    public void init() {

        final @NotNull List<Role> currentRoles = configuration.getCurrentRoles();
        if (!currentRoles.isEmpty()) {
            updateRolesMap(currentRoles);
        }

        configuration.addReloadCallback((newConfig) -> updateRolesMap(newConfig));
    }

    /**
     * @param userName the userName
     * @param password the password - in this case the Oauth2 access token
     * @return a set of the users roles or empty set if the credentials are not valid
     */
    @Nullable
    public Set<String> getRoles(@NotNull final String userName, @NotNull final ByteBuffer password) {
        //If Config is invalid do not allow clients to connect
        if (roles.isEmpty()) {
            return Collections.emptySet();
        }

        final Lock readLock = oidcServerLock.readLock();
        readLock.lock();
        final Set<String> userRoles;
        try {
            byte[] passwordBytes = new byte[password.remaining()];
            password.get(passwordBytes);
            final String jwtToken = new String(passwordBytes);
            userRoles = oauth2AccessTokenHandler.getResourceAccessRoles(jwtToken);
        } finally {
            readLock.unlock();
        }

        return userRoles;
    }

    @NotNull
    public List<TopicPermission> getPermissions(final @NotNull String clientId, final @NotNull String userName, final @NotNull Set<String> clientRoles) {
        final ArrayList<TopicPermission> topicPermissions = new ArrayList<>();
        for (String clientRole : clientRoles) {
            final Role role = roles.get(clientRole);
            if (role != null) {
                for (Permission permission : role.getPermissions()) {
                    topicPermissions.add(toTopicPermission(clientId, userName, permission));
                }
            }
        }
        return topicPermissions;
    }

    private void updateRolesMap(final @NotNull List<Role> newRoles) {

        final ConcurrentHashMap<String, Role> newRolesMap = new ConcurrentHashMap<>(newRoles.size());
        for (Role newRole : newRoles) {
            newRolesMap.put(newRole.getId(), newRole);
        }

        final Lock writeLock = rolesLock.writeLock();
        writeLock.lock();
        try {
            roles = newRolesMap;
            log.info("roles map has been updated");
        } finally {
            writeLock.unlock();
        }
    }

    private TopicPermission toTopicPermission(@NotNull final String clientId, @NotNull final String userName, @NotNull final Permission permission) {
        return Builders.topicPermission()
                .topicFilter(getTopicFilter(clientId, userName, permission))
                .activity(permission.getActivity())
                .type(TopicPermission.PermissionType.ALLOW)
                .retain(permission.getRetain())
                .qos(permission.getQos())
                .sharedSubscription(permission.getSharedSubscription())
                .sharedGroup(permission.getSharedGroup())
                .build();
    }

    private String getTopicFilter(@NotNull final String clientId, @NotNull final String userName, final Permission permission) {
        final String configTopic = permission.getTopic();
        return Substitution.substitute(configTopic, clientId, userName);
    }

}
