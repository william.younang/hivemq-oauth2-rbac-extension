/*
 * Copyright 2018 dc-square GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.hivemq.extensions.rbac.configuration;

import com.hivemq.extension.sdk.api.annotations.NotNull;
import com.hivemq.extensions.rbac.configuration.entities.Permission;
import com.hivemq.extensions.rbac.configuration.entities.Role;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class ConfigRbacValidator {

    @NotNull
    public static ValidationResult validateConfig(@NotNull final List<Role> roles) {

        final List<String> errors = new ArrayList<>();
        boolean validationSuccessful = true;

        if (roles == null || roles.isEmpty()) {
            errors.add("No Roles found in configuration table");
            validationSuccessful = false;
        }

        //if users or roles are missing stop here
        if (!validationSuccessful) {
            return new ValidationResult(errors, false);
        }

        Set<String> roleIds = new HashSet<>();

        for (Role role : roles) {
            if (role.getId() == null || role.getId().isEmpty()) {
                errors.add("A Role is missing an ID");
                validationSuccessful = false;
                continue;
            }

            if (roleIds.contains(role.getId())) {
                errors.add("Duplicate ID '" + role.getId() + "' for role");
                validationSuccessful = false;
                continue;
            }

            roleIds.add(role.getId());

            if (role.getPermissions() == null || role.getPermissions().isEmpty()) {
                errors.add("Role '" + role.getId() + "' is missing permissions");
                validationSuccessful = false;
                continue;
            }

            for (Permission permission : role.getPermissions()) {
                if (permission.getTopic() == null || permission.getTopic().isEmpty()) {
                    errors.add("A Permission for role with id '" + role.getId() + "' is missing a topic filter");
                    validationSuccessful = false;
                }

                if (permission.getActivity() == null) {
                    errors.add("Invalid value for Activity in Permission for role with id '" + role.getId() + "'");
                    validationSuccessful = false;
                }

                if (permission.getQos() == null) {
                    errors.add("Invalid value for QoS in Permission for role with id '" + role.getId() + "'");
                    validationSuccessful = false;
                }

                if (permission.getRetain() == null) {
                    errors.add("Invalid value for Retain in Permission for role with id '" + role.getId() + "'");
                    validationSuccessful = false;
                }

                if (permission.getSharedGroup() == null || permission.getSharedGroup().isEmpty()) {
                    errors.add("Invalid value for Shared Group in Permission for role with id '" + role.getId() + "'");
                    validationSuccessful = false;
                }

                if (permission.getSharedSubscription() == null) {
                    errors.add("Invalid value for Shared Subscription in Permission for role with id '" + role.getId() + "'");
                    validationSuccessful = false;
                }
            }
        }

        return new ValidationResult(errors, validationSuccessful);
    }

    public static class ValidationResult {
        private final @NotNull List<String> errors;
        private final boolean validationSuccessful;

        private ValidationResult(@NotNull final List<String> errors, final boolean validationSuccessful) {
            this.errors = errors;
            this.validationSuccessful = validationSuccessful;
        }

        @NotNull
        public List<String> getErrors() {
            return errors;
        }

        public boolean isValidationSuccessful() {
            return validationSuccessful;
        }
    }
}
