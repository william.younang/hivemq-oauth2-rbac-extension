package com.hivemq.extensions.rbac.configuration.entities;

/**
 * @author William Younang
 **/
public class OAuth2Config {
    private String oauth2ServerUrl;

    private String oauth2RealmId;

    private String oauth2ClientId;

    public OAuth2Config(String oauth2ServerUrl, String oauth2RealmId, String oauth2ClientId) {
        this.oauth2ServerUrl = oauth2ServerUrl;
        this.oauth2RealmId = oauth2RealmId;
        this.oauth2ClientId = oauth2ClientId;
    }

    public String getOauth2ServerUrl() {
        return oauth2ServerUrl;
    }

    public String getOauth2RealmId() {
        return oauth2RealmId;
    }

    public String getOauth2ClientId() {
        return oauth2ClientId;
    }

}
