/*
 * Copyright 2018 dc-square GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.hivemq.extensions.rbac.configuration.entities;

import com.hivemq.extension.sdk.api.annotations.NotNull;

public class ExtensionConfig {

    private int reloadInterval;

    private OAuth2Config oAuth2Config;

    public ExtensionConfig() {
    }

    public OAuth2Config getoAuth2Config() {
        return oAuth2Config;
    }

    public int getReloadInterval() {
        return reloadInterval;
    }

    public void setReloadInterval(final int reloadInterval) {
        this.reloadInterval = reloadInterval;
    }

    public void init() throws Exception {
        this.reloadInterval = Integer.parseInt(System.getenv("RBAC_CONFIG_RELOAD_INTERVAL"));
        if (reloadInterval <= 0) {
            throw new Exception("RBAC_CONFIG_RELOAD_INTERVAL must be a positive integer");
        }

        this.oAuth2Config = new OAuth2Config(System.getenv("AUTH2_SERVER_URL"), System.getenv("AUTH2_REALM_ID"),
                System.getenv("AUTH2_CLIENT_ID"));
    }

    @NotNull
    @Override
    public String toString() {
        return "ExtensionConfiguration{" +
                "reloadInterval=" + reloadInterval +
                '}';
    }
}
