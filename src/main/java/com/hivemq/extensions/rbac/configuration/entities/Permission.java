/*
 * Copyright 2018 dc-square GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.hivemq.extensions.rbac.configuration.entities;

import com.hivemq.extension.sdk.api.annotations.NotNull;
import com.hivemq.extension.sdk.api.annotations.Nullable;
import com.hivemq.extension.sdk.api.auth.parameter.TopicPermission;

public class Permission {

    private String topic;

    private TopicPermission.MqttActivity activity = TopicPermission.MqttActivity.ALL;

    private TopicPermission.Qos qos = TopicPermission.Qos.ALL;

    private TopicPermission.Retain retain = TopicPermission.Retain.ALL;

    private TopicPermission.SharedSubscription sharedSubscription = TopicPermission.SharedSubscription.ALL;

    private String sharedGroup = "#";

    public Permission() {
    }

    public Permission(@Nullable final String topic) {
        this.topic = topic;
    }

    @Nullable
    public String getTopic() {
        return topic;
    }

    public void setTopic(@NotNull final String topic) {
        this.topic = topic;
    }

    @NotNull
    public TopicPermission.MqttActivity getActivity() {
        return activity;
    }

    public void setActivity(@NotNull final TopicPermission.MqttActivity activity) {
        this.activity = activity;
    }

    @NotNull
    public TopicPermission.Qos getQos() {
        return qos;
    }

    public void setQos(@NotNull final TopicPermission.Qos qos) {
        this.qos = qos;
    }

    @NotNull
    public TopicPermission.Retain getRetain() {
        return retain;
    }

    public void setRetain(@NotNull final TopicPermission.Retain retain) {
        this.retain = retain;
    }

    @NotNull
    public TopicPermission.SharedSubscription getSharedSubscription() {
        return sharedSubscription;
    }

    public void setSharedSubscription(@NotNull final TopicPermission.SharedSubscription sharedSubscription) {
        this.sharedSubscription = sharedSubscription;
    }

    @NotNull
    public String getSharedGroup() {
        return sharedGroup;
    }

    public void setSharedGroup(@NotNull final String sharedGroup) {
        this.sharedGroup = sharedGroup;
    }

    @NotNull
    @Override
    public String toString() {
        return "Permission{" +
                "topic='" + topic + '\'' +
                ", activity=" + activity +
                ", qos=" + qos +
                ", retain=" + retain +
                ", sharedSubscription=" + sharedSubscription +
                ", sharedGroup='" + sharedGroup + '\'' +
                '}';
    }
}
