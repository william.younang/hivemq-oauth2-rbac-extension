/*
 * Copyright 2018 dc-square GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.hivemq.extensions.rbac.configuration;

import com.hivemq.extension.sdk.api.annotations.NotNull;
import com.hivemq.extension.sdk.api.annotations.ThreadSafe;
import com.hivemq.extensions.rbac.configuration.entities.ExtensionConfig;
import com.hivemq.extensions.rbac.configuration.entities.Role;
import com.hivemq.extensions.rbac.repository.RbacConfigRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.Queue;
import java.util.concurrent.*;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

import static java.util.Collections.unmodifiableList;


@ThreadSafe
public class Configuration {

    private static final Logger log = LoggerFactory.getLogger(Configuration.class);

    private final ReadWriteLock lock = new ReentrantReadWriteLock();

    // COWAL is perfect here because the callbacks are not expected to change regularly
    //The callbacks are shared between this class and the reloadable config task. Modifications are only possible via this class")
    private final List<ReloadCallback> callbacks = new CopyOnWriteArrayList<>();

    //"This queue is thread safe and shared between this class and the reloadable config file task")
    private final Queue<ReloadCallback> newCallbacks = new LinkedBlockingQueue<>();

    private List<Role> currentRoles;

    //guarded by lock
    private final @NotNull RbacConfigRepository rbacConfigRepository;


    public Configuration(@NotNull final RbacConfigRepository rbacConfigRepository,
                         @NotNull final ScheduledExecutorService extensionExecutorService,
                         @NotNull final ExtensionConfig extensionConfig) {

        this.rbacConfigRepository = rbacConfigRepository;
        final ReloadConfigFileTask reloadableTask = new ReloadConfigFileTask(
                extensionExecutorService,
                unmodifiableList(callbacks) /* We don't want the task to modify the callbacks!*/,
                newCallbacks,
                rbacConfigRepository);
        extensionExecutorService.scheduleWithFixedDelay(reloadableTask, extensionConfig.getReloadInterval(),
                extensionConfig.getReloadInterval(), TimeUnit.SECONDS);
    }

    public void init() {

        currentRoles = rbacConfigRepository.getAllRoles();

        if (currentRoles.isEmpty()) {
            log.warn("No rbac configuration available, denying all connections.");
        }

        addReloadCallback((newRoles) -> {
            final Lock writeLock = lock.writeLock();
            writeLock.lock();
            try {
                currentRoles = newRoles;
            } finally {
                writeLock.unlock();
            }
        });
    }

    /**
     * Adds a reload callback.
     */
    public void addReloadCallback(@NotNull final ReloadCallback callback) {
        callbacks.add(callback);
        newCallbacks.add(callback);
    }


    /**
     * A callback that gets triggered every time the config file changes.
     * <p>
     * A callback is guaranteed to not get executed concurrently.
     * <p>
     * All callbacks are executed in a dedicated executor. Callbacks are not executed concurrently,
     * so make sure they don't block for too long
     */
    public interface ReloadCallback {

        /**
         * An callback that is called when the config changes
         *
         * @param newRoles the new config
         */
        void onReload(@NotNull List<Role> newRoles);

    }

    public List<Role> getCurrentRoles() {
        return currentRoles;
    }

    private static class ReloadConfigFileTask implements Runnable {

        private final @NotNull ExecutorService callbackExecutor;
        private final @NotNull RbacConfigRepository rbacConfigRepository;
        private final @NotNull List<ReloadCallback> callbacks;
        private final @NotNull Queue<ReloadCallback> newCallbacks;

        ReloadConfigFileTask(@NotNull final ExecutorService callbackExecutor,
                             @NotNull final List<ReloadCallback> callbacks,
                             @NotNull final Queue<ReloadCallback> newCallbacks,
                             final @NotNull RbacConfigRepository rbacConfigRepository) {

            this.callbacks = callbacks;
            this.newCallbacks = newCallbacks;
            this.callbackExecutor = callbackExecutor;
            this.rbacConfigRepository = rbacConfigRepository;
        }

        @Override
        public void run() {

            log.info("reload config is called");

            List<Role> roles = rbacConfigRepository.getAllRoles();
            if (roles.isEmpty()) {
                log.error("There is no configuration for rbac in the configuration table");
                return;
            }

            log.info("loaded rbac rules {} roles", roles.size());

            for (ReloadCallback callback : callbacks) {
                callback.onReload(roles);
            }
        }
    }
}
