package com.hivemq.extensions.rbac.configuration.entities;

import com.hivemq.extension.sdk.api.annotations.NotNull;

/**
 * @author William Younang
 **/
public class DatabaseConfig {

    private String hosts;

    private String databaseUserName;

    private String databasePassword;

    private String databaseTable;

    public DatabaseConfig() {
    }

    public DatabaseConfig(@NotNull String hosts, @NotNull String databaseUserName, @NotNull String databasePassword, @NotNull String databaseTable) {
        this.hosts = hosts;
        this.databaseUserName = databaseUserName;
        this.databasePassword = databasePassword;
        this.databaseTable = databaseTable;
    }

    public String getDatabaseUserName() {
        return databaseUserName;
    }

    public void setDatabaseUserName(String databaseUserName) {
        this.databaseUserName = databaseUserName;
    }

    public String getDatabasePassword() {
        return databasePassword;
    }

    public void setDatabasePassword(String databasePassword) {
        this.databasePassword = databasePassword;
    }

    public String getHosts() {
        return hosts;
    }

    public void setHosts(String hosts) {
        this.hosts = hosts;
    }

    public String getDatabaseTable() {
        return databaseTable;
    }

    public void setDatabaseTable(String databaseTable) {
        this.databaseTable = databaseTable;
    }
}
