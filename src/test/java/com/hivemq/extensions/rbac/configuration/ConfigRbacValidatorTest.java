/*
 * Copyright 2018 dc-square GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.hivemq.extensions.rbac.configuration;

import com.hivemq.extension.sdk.api.annotations.NotNull;
import com.hivemq.extensions.rbac.configuration.ConfigRbacValidator.ValidationResult;
import com.hivemq.extensions.rbac.configuration.entities.Permission;
import com.hivemq.extensions.rbac.configuration.entities.Role;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;


public class ConfigRbacValidatorTest {

    @Test
    public void test_no_roles() {

        final ValidationResult result = ConfigRbacValidator.validateConfig(null);

        assertFalse(result.isValidationSuccessful());

        checkErrorString(result, "No Roles found in configuration table");
    }

    @Test
    public void test_empty_roles() {

        final ValidationResult result = ConfigRbacValidator.validateConfig(List.of());

        assertFalse(result.isValidationSuccessful());

        checkErrorString(result, "No Roles found in configuration table");
    }

    @Test
    public void test_missing_id() {

        final ValidationResult result = ConfigRbacValidator.validateConfig(List.of(new Role(null, List.of(new Permission("topic")))));

        assertFalse(result.isValidationSuccessful());
        checkErrorString(result, "A Role is missing an ID");
    }

    @Test
    public void test_empty_id() {

        final ValidationResult result = ConfigRbacValidator.validateConfig(List.of(new Role("", List.of(new Permission("topic")))));

        assertFalse(result.isValidationSuccessful());
        checkErrorString(result, "A Role is missing an ID");
    }

    @Test
    public void test_duplicate_id() {

        final List<Role> roles = List.of(
                new Role("1", List.of(new Permission("topic"))),
                new Role("1", List.of(new Permission("topic")))
        );

        final ValidationResult result = ConfigRbacValidator.validateConfig(roles);

        assertFalse(result.isValidationSuccessful());
        checkErrorString(result, "Duplicate ID '1' for role");
    }

    @Test
    public void test_no_permissions() {

        final List<Role> roles = List.of(
                new Role("1", List.of(new Permission("topic"))),
                new Role("2", List.of())
        );
        final ValidationResult result = ConfigRbacValidator.validateConfig(roles);

        assertFalse(result.isValidationSuccessful());
        checkErrorString(result, "Role '2' is missing permissions");
    }

    @Test
    public void test_null_permissions() {

        List<Role> roles = List.of(
                new Role("1", List.of(new Permission("topic"))),
                new Role("2", null)
        );
        final ValidationResult result = ConfigRbacValidator.validateConfig(roles);

        assertFalse(result.isValidationSuccessful());
        checkErrorString(result, "Role '2' is missing permissions");
    }

    @Test
    public void test_permission_no_topic() {

        final List<Role> roles = List.of(
                new Role("1", List.of(new Permission(null)))
        );

        final ValidationResult result = ConfigRbacValidator.validateConfig(roles);

        assertFalse(result.isValidationSuccessful());
        checkErrorString(result, "A Permission for role with id '1' is missing a topic filter");
    }

    @Test
    public void test_permission_no_activity() {

        final Permission permission = new Permission("abc");
        permission.setActivity(null);
        final List<Role> roles = List.of(
                new Role("1", List.of(permission))
        );

        final ValidationResult result = ConfigRbacValidator.validateConfig(roles);

        assertFalse(result.isValidationSuccessful());
        checkErrorString(result, "Invalid value for Activity in Permission for role with id '1'");
    }

    @Test
    public void test_permission_no_qos() {

        final Permission permission = new Permission("abc");
        permission.setQos(null);
        final List<Role> roles = List.of(
                new Role("1", List.of(permission))
        );

        final ValidationResult result = ConfigRbacValidator.validateConfig(roles);

        assertFalse(result.isValidationSuccessful());
        checkErrorString(result, "Invalid value for QoS in Permission for role with id '1'");
    }

    @Test
    public void test_permission_no_retain() {
        final Permission permission = new Permission("abc");
        permission.setRetain(null);
        final List<Role> roles = List.of(
                new Role("1", List.of(permission))
        );
        final ValidationResult result = ConfigRbacValidator.validateConfig(roles);

        assertFalse(result.isValidationSuccessful());
        checkErrorString(result, "Invalid value for Retain in Permission for role with id '1'");
    }

    @Test
    public void test_permission_no_sharedGroup() {

        final Permission permission = new Permission("abc");
        permission.setSharedGroup(null);
        final List<Role> roles = List.of(
                new Role("1", List.of(permission))
        );

        final ValidationResult result = ConfigRbacValidator.validateConfig(roles);

        assertFalse(result.isValidationSuccessful());
        checkErrorString(result, "Invalid value for Shared Group in Permission for role with id '1'");
    }

    @Test
    public void test_permission_no_sharedSub() {

        final Permission permission = new Permission("abc");
        permission.setSharedSubscription(null);
        final List<Role> roles = List.of(
                new Role("1", List.of(permission))
        );

        final ValidationResult result = ConfigRbacValidator.validateConfig(roles);

        assertFalse(result.isValidationSuccessful());
        checkErrorString(result, "Invalid value for Shared Subscription in Permission for role with id '1'");
    }

    @Test
    public void test_valid_hashed_pw_username() {

        final List<Role> roles = List.of(new Role("1", List.of(new Permission("topic"))));

        final ValidationResult result = ConfigRbacValidator.validateConfig(roles);

        assertTrue(result.isValidationSuccessful());
    }

    private void checkErrorString(final ValidationResult result, @NotNull final String s) {
        assertTrue("Wanted error reason \"" + s + "\" not contained in: " + result.getErrors().toString(),
                result.getErrors().contains(s));
    }

}