package com.hivemq.extensions.rbac.utils;

import com.hivemq.extensions.rbac.configuration.Configuration;
import com.hivemq.extensions.rbac.configuration.entities.Permission;
import com.hivemq.extensions.rbac.configuration.entities.Role;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.nio.ByteBuffer;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

public class CredentialsValidatorTest {

    @Mock
    private Configuration configuration;

    @Mock
    private Oauth2AccessTokenHandler oauth2AccessTokenHandler;

    private CredentialsValidator credentialsValidator;

    @Before
    public void before() {
        MockitoAnnotations.initMocks(this);

        credentialsValidator = new CredentialsValidator(configuration, oauth2AccessTokenHandler);

        List<Role> roles = List.of(new Role("id1", List.of(new Permission("topic1"))));
        when(configuration.getCurrentRoles()).thenReturn(roles);
        credentialsValidator.init();
        verify(configuration, times(1)).addReloadCallback(any(Configuration.ReloadCallback.class));
    }

    @Test
    public void getRoles() {
        String token = "eyJhbGciOiJSUzI1NiIsInR5cCIgOiAiSldUIiwia2lkIiA6ICJ5ZzhWR3I4ZzRfa2F6RE9zMXBUTUFzUUpIU0plQXhjUzJyRzVrMnpIczVJIn0.eyJleHAiOjE1OTk4NTc2MDksImlhdCI6MTU5OTg1NjQwOCwianRpIjoiZjJiNTZiNDktNjQ0NC00YjgxLThmNmQtZmY5OTQ2ZjRjMTdiIiwiaXNzIjoiaHR0cDovL2xvY2FsaG9zdDo4MDgwL2F1dGgvcmVhbG1zL3lvdW5hbmdsYWIiLCJhdWQiOlsicmVhbG0tbWFuYWdlbWVudCIsImFjY291bnQiXSwic3ViIjoiMjU4NjQ3YmItNDg0OS00ZDFmLThhMTUtMzZhODU1MTA4NmZlIiwidHlwIjoiQmVhcmVyIiwiYXpwIjoiaHVtYW4tY2xpbmljIiwic2Vzc2lvbl9zdGF0ZSI6IjAzZTVjYzA5LWVmZmEtNGFjZi04MjZlLTFmYWJkOTlmMDYxYyIsImFjciI6IjEiLCJhbGxvd2VkLW9yaWdpbnMiOlsiaHR0cDovL2xvY2FsaG9zdDo0MjAwIl0sInJlYWxtX2FjY2VzcyI6eyJyb2xlcyI6WyJvZmZsaW5lX2FjY2VzcyIsInVtYV9hdXRob3JpemF0aW9uIl19LCJyZXNvdXJjZV9hY2Nlc3MiOnsicmVhbG0tbWFuYWdlbWVudCI6eyJyb2xlcyI6WyJtYW5hZ2UtdXNlcnMiLCJ2aWV3LXVzZXJzIiwicXVlcnktZ3JvdXBzIiwicXVlcnktdXNlcnMiXX0sImh1bWFuLWNsaW5pYyI6eyJyb2xlcyI6WyJ2aWV3LXVzZXJzIiwiQURNSU4iLCJVU0VSIiwiRE9DVE9SIl19LCJhY2NvdW50Ijp7InJvbGVzIjpbIm1hbmFnZS1hY2NvdW50IiwibWFuYWdlLWFjY291bnQtbGlua3MiLCJ2aWV3LXByb2ZpbGUiXX19LCJzY29wZSI6InByb2ZpbGUgZW1haWwiLCJlbWFpbF92ZXJpZmllZCI6dHJ1ZSwidXNlcl9uYW1lIjoid3lvdW5hbmciLCJuYW1lIjoiV2lsbGlhbSBZT1VOQU5HIiwicHJlZmVycmVkX3VzZXJuYW1lIjoid3lvdW5hbmciLCJnaXZlbl9uYW1lIjoiV2lsbGlhbSIsImZhbWlseV9uYW1lIjoiWU9VTkFORyIsImVtYWlsIjoid2lsbGlhbXlvdW5hbmdAZ21haWwuY29tIn0.EcnUsoMkKemsIrjv2rI2EjxPt-MpHNhQQK2k7hMR2wH9tzTyTypaUJyU6VA75755fubjCv6Ea7r__nIOZ_mYEK46TS8gG9Z5PGg2vXVzoegCNN0Iv2W54P_I4zCMRhbtD5cxlrBcHTBbYWRUcTXjgMWL-SN8jkgmctdV8TRCOhT9k7IGfI6csqD5SWBcgyzVf7C575gMqZtE0kwX_r6g-W5T6ieuDyoS967L6npfoDderLU1jf9JbZGYuyAtdROsthvTCRykyDSsD9K8G6JoQl8m5pGtvDr0AEoPdVDx2COFXp1dXGwJvfcLSeo-FlMRu868vco2lG2EXZAvNATkEA";

        when(oauth2AccessTokenHandler.getResourceAccessRoles(anyString())).thenReturn(new HashSet<>(Collections.singletonList("id1")));

        Set<String> roles = credentialsValidator.getRoles("myUserName", ByteBuffer.wrap(token.getBytes()));
        assertEquals(1, roles.size());
    }

    @Test(expected = RuntimeException.class)
    public void getPermissions() {
        credentialsValidator.getPermissions("clientId", "myUserName", new HashSet<>(Collections.singletonList("id1")));
    }
}