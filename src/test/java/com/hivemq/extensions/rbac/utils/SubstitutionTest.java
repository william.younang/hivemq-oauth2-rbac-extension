package com.hivemq.extensions.rbac.utils;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class SubstitutionTest {

    private final Substitution substitution = new Substitution();

    @Test
    public void substitute_username() {
        final String realTopic = Substitution.substitute("incoming/${{username}}/actions", "mio", "wyou");
        assertEquals("incoming/wyou/actions", realTopic);
    }

    @Test
    public void substitute_clientId() {
        final String realTopic = Substitution.substitute("incoming/${{clientid}}/actions", "mio", "wyou");
        assertEquals("incoming/mio/actions", realTopic);
    }
}