package com.hivemq.extensions.rbac.repository;

import com.hivemq.extensions.rbac.configuration.entities.DatabaseConfig;
import com.hivemq.extensions.rbac.configuration.entities.Role;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.assertFalse;

public class CouchbaseRbacConfigRepositoryTest {

    @Test
    public void getAllRoles() {
        final DatabaseConfig databaseConfig = new DatabaseConfig("192.168.1.53", "hivemq-rbac", "ChangeThisBeforeProd", "hivemq_oauth2_rbac_config");

        CouchbaseRbacConfigRepository repository = new CouchbaseRbacConfigRepository(databaseConfig);

        List<Role> roles = repository.getAllRoles();

        assertFalse(roles.isEmpty());
    }
}